# jmmc-obsportal-kubernetes

Deployement requires some env vars and a dedicated log subdirectory so we connect to the postgres server using a login/password info and do not mix log files accross BLUE or GREEN variant. Volumes and ingresses must be set before.


Common k8s note for jmmc pods:
- define default namespace to jmmc:
    kubectl config set-context --current --namespace=jmmc


Secrets must be present or created with:
- kubectl create secret generic obsportal-secret-blue  --from-literal=OBSPORTAL_SQLALCHEMY_URL=postgresql://user:password@host/dbname?client_encoding=utf8
- kubectl create secret generic obsportal-secret-green --from-literal=OBSPORTAL_SQLALCHEMY_URL=postgresql://user:password@host/dbname?client_encoding=utf8


* HTTP ingress:
Edit blue-green-ingress.yml to map obs and obs-preprod backend to the right color service (=> switch color in serviceName entries ) . Apply changes using next command:
* kubectl apply -f blue-green-ingress.yml

An history of past commands tracks changes. Please record them using next command :
- cp blue-green-ingress.yml  kubectl.history/blue-green-ingress.$(date +"%F_%T").yml


To update the deployed application, 
* get the TAG from gricad gitlab registry (docker image manually build):
https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/JMMC/jmmc-obsportal/container_registry
* change the TAG in kustomization.yml files:
- meld overlays/green/kustomization.yml overlays/blue/kustomization.yml 


Following step should help to make a clean deployment:
* BLUE instance:
- kustomize build overlays/blue  | kubectl apply -f -
- VARIANT=blue;  kustomize build overlays/$VARIANT > kubectl.history/$VARIANT.$(date +"%F_%T").yml

* GREEN instance:
- kustomize build overlays/green | kubectl apply -f -
- VARIANT=green; kustomize build overlays/$VARIANT > kubectl.history/$VARIANT.$(date +"%F_%T").yml


Some more sample commands :
- kubectl get pods                          # to list and check your application pod
- kubectl get cronjobs                      # to list and check your application cronjobs

- kubectl delete pod obsportal-XXX          # to respawned a new instance on demand ( at present image is tagged latest so it always use the last build )
- kubectl exec -it obsportal-XXX bash       # to run some cli operations if required

-  kubectl logs --tail=100 obsportal-XXX   # to get last 100 lines from your application pod's stdout logs

If no cronjob has been set, you can run import manually within kubernetes:
- kubectl exec obsportal-XXX -- bash -c "import-db.sh > /dev/null 2> /dev/null &"

Code snipets for testing :
- check for constant backend response for a given ingress
 - while true; do if VARIANT=$(curl -s obs-preprod.jmmc.fr/health | grep -A1 Variant); then echo ${VARIANT}; else echo "ERROR no variant found" ; fi ;  done
 - while true; do if VARIANT=$(curl -s obs.jmmc.fr/health | grep -A1 Variant); then echo ${VARIANT}; else echo "ERROR no variant found" ; fi ;  done
